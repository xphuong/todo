package com.phuongle.todo.database.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.phuongle.todo.database.entity.Todo

@Dao
interface TodoDao {

    @Query("SELECT * FROM todo ORDER BY completed ASC")
    fun getAllTodoList(): LiveData<List<Todo>>

    @Query("SELECT * FROM todo WHERE completed = :completed ORDER BY title ASC")
    fun getTodoList(completed: Boolean): LiveData<List<Todo>>

    @Insert
    suspend fun add(todoRecord: Todo)

    @Delete
    suspend fun delete(todoRecord: Todo)

    @Update
    suspend fun update(todoRecord: Todo)

}