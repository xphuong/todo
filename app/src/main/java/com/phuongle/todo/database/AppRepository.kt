package com.phuongle.todo.database

import android.app.Application
import androidx.lifecycle.LiveData
import com.phuongle.todo.database.entity.Todo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class AppRepository(application: Application) {

    private val database = AppDatabase.getInstance(application.applicationContext)
    private val todoDao = database!!.todoDao()

    fun getTodoList(isAll: Boolean, completed: Boolean): LiveData<List<Todo>> {
        return if (isAll) todoDao.getAllTodoList()
        else todoDao.getTodoList(completed)
    }

    fun add(todo: Todo) = runBlocking {
        this.launch(Dispatchers.IO) {
            todoDao.add(todo)
        }
    }

    fun update(todo: Todo) = runBlocking {
        this.launch(Dispatchers.IO) {
            todoDao.update(todo)
        }
    }

    fun delete(todo: Todo) {
        runBlocking {
            this.launch(Dispatchers.IO) {
                todoDao.delete(todo)
            }
        }
    }

}