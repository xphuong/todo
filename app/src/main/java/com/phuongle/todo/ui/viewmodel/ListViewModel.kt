package com.phuongle.todo.ui.viewmodel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.phuongle.todo.database.AppRepository
import com.phuongle.todo.database.entity.Todo

open class ListViewModel(
    application: Application,
    private var isAll: Boolean,
    private var completed: Boolean
) :
    AndroidViewModel(application) {

    private val repository: AppRepository = AppRepository(application)

    fun save(todo: Todo, isNew: Boolean) {
        if (isNew) add(todo)
        else update(todo)
    }

    private fun add(todo: Todo) {
        repository.add(todo)
    }

    private fun update(todo: Todo) {
        repository.update(todo)
    }

    fun delete(todo: Todo) {
        repository.delete(todo)
    }

    fun filter(): LiveData<List<Todo>> {
        return repository.getTodoList(isAll, completed)
    }

}