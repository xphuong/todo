package com.phuongle.todo.ui

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.Handler
import android.view.*
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.phuongle.todo.R
import com.phuongle.todo.database.entity.Todo
import com.phuongle.todo.ui.viewmodel.ListViewModel
import kotlinx.android.synthetic.main.fragment_todo_list.view.*

open class BaseFragment : Fragment() {

    lateinit var mViewModel: ListViewModel

    private var mContext: Context? = null
    private lateinit var mAdapter: Adapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_todo_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mContext = this.context

        initViews(view)
        filter()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.toolbar_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_add -> showTodoInfoDialog(null)
        }

        return super.onOptionsItemSelected(item)
    }

    private fun initViews(view: View) {
        mAdapter = Adapter(object :
            Adapter.OnItemListener {
            override fun onCompleteClicked(todo: Todo, completed: Boolean) {
                mViewModel.save(Todo(todo.id, todo.title, todo.content, completed), false)
            }

            override fun onItemClicked(item: Todo) {
                showTodoInfoDialog(item)
            }

            override fun onMoreClicked(item: Todo) {
                showMoreDialog(item)
            }
        })
        view.rv_items.layoutManager = LinearLayoutManager(mContext!!)
        view.rv_items.adapter = mAdapter
    }

    private fun filter() {
        mViewModel.filter().observe(this, Observer { mAdapter.setItems(it) })
    }

    private fun showTodoInfoDialog(todo: Todo?) {
        val dialogView: View =
            layoutInflater.inflate(R.layout.layout__bottom_sheet__add_new_todo, null)
        val dialog = BottomSheetDialog(mContext!!)
        dialog.setContentView(dialogView)

        val edtTitle = dialogView.findViewById<EditText>(R.id.edt_title)
        val edtContent = dialogView.findViewById<EditText>(R.id.edt_content)

        edtTitle.setText(todo?.title)
        edtContent.setText(todo?.content)

        val btnCancel = dialogView.findViewById<Button>(R.id.btn_cancel)
        val btnDone = dialogView.findViewById<Button>(R.id.btn_done)

        btnCancel.setOnClickListener { dialog.dismiss() }
        btnDone.setOnClickListener {
            var valid = true

            if (edtTitle.text.isEmpty()) {
                edtTitle.error = "Please fill the title"
                edtTitle.requestFocus()
                valid = false
            }

            if (edtContent.text.isEmpty()) {
                edtContent.error = "Please fill the content"
                edtContent.requestFocus()
                valid = false
            }

            if (valid) {
                mViewModel.save(
                    Todo(
                        todo?.id,
                        edtTitle.text.toString().trim(),
                        edtContent.text.toString().trim(),
                        todo?.completed ?: false
                    ), todo == null
                )

                dialog.dismiss()
            }
        }

        Handler().postDelayed({ edtTitle.requestFocus() }, 500)

        dialog.show()
    }

    private fun showMoreDialog(todo: Todo) {
        AlertDialog.Builder(mContext!!)
            .setItems(arrayOf(getString(R.string.text__menu__delete))) { dialogInterface: DialogInterface, i: Int ->
                when (i) {
                    0 -> {
                        mViewModel.delete(todo)
                        Toast.makeText(
                            mContext!!,
                            getString(R.string.message_remove_success),
                            Toast.LENGTH_SHORT
                        )
                            .show()
                    }
                }
            }.show()
    }

}