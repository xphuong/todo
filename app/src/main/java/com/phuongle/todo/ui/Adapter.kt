package com.phuongle.todo.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.phuongle.todo.R
import com.phuongle.todo.database.entity.Todo
import kotlinx.android.synthetic.main.item__todo.view.*

class Adapter(private var listener: OnItemListener) : RecyclerView.Adapter<Adapter.ViewHolder>() {

    interface OnItemListener {
        fun onItemClicked(item: Todo)
        fun onCompleteClicked(todo: Todo, completed: Boolean)
        fun onMoreClicked(item: Todo)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(todo: Todo, listener: OnItemListener) {
            itemView.tv_title.text = todo.title
            itemView.tv_content.text = todo.content
            itemView.chk_completed.isChecked = todo.completed

            itemView.chk_completed.setOnClickListener {
                listener.onCompleteClicked(
                    todo,
                    !todo.completed
                )
            }
            itemView.layout_item.setOnClickListener { listener.onItemClicked(todo) }
            itemView.iv_more.setOnClickListener { listener.onMoreClicked(todo) }
        }
    }

    private var todoList: List<Todo> = emptyList()
    private var displayTodoList: List<Todo> = emptyList()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item__todo, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(displayTodoList[position], listener)
    }

    override fun getItemCount(): Int = displayTodoList.size

    fun setItems(items: List<Todo>) {
        this.todoList = items
        this.displayTodoList = items
        notifyDataSetChanged()
    }

}