package com.phuongle.todo.ui.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.phuongle.todo.ui.BaseFragment
import com.phuongle.todo.ui.viewmodel.ListViewModel

class InCompleteFragment : BaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mViewModel = ListViewModel(this.activity!!.application, isAll = false, completed = false)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

}